from django.conf.urls import url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^areas/$',views.AreasView.as_view()),
    url(r'^areas/(?P<pk>\d+)/$',views.AreasCityView.as_view()),
    url(r'^addresses/$', views.AddAreasView.as_view()),
    url(r'^addresses/(?P<pk>\d+)/$', views.DetailAreasView.as_view()),
    url(r'^addresses/(?P<pk>\d+)/title/$', views.DetailView.as_view()),
    url(r'^addresses/(?P<pk>\d+)/status/$', views.DefaultView.as_view()),

]