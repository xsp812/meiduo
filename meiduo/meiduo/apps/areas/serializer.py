import re

from rest_framework import serializers

from areas.models import Area, Address


class AreaSerializer(serializers.ModelSerializer):
    """地区序列化"""
    class Meta:
        model = Area
        fields=('id','name')

class DetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        fields=('id','title')

class AddressSerializer(serializers.ModelSerializer):
    province = serializers.StringRelatedField(label='省', read_only=True)
    city = serializers.StringRelatedField(label='市', read_only=True)
    district = serializers.StringRelatedField(label='区', read_only=True)

    city_id = serializers.IntegerField(write_only=True)
    district_id = serializers.IntegerField(write_only=True)
    province_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Address
        exclude = ('user', 'is_deleted', 'create_time', 'update_time')

    def validate(self, attrs):
        if not re.match(r'^1[3-9]\d{9}$', attrs['mobile']):
            raise serializers.ValidationError('asdqweknm')
        return attrs

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data['user'] = user

        addres = super().create(validated_data)

        return addres

class DetailAddressSerializer(serializers.Serializer):
        pass

