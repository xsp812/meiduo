from django.shortcuts import render
from django_redis import get_redis_connection
from rest_framework.response import Response
from rest_framework.views import APIView
import random
from meiduo.libs.yuntongxun.sms import CCP
from celery_tasks.sms.tasks import send_sms_code
# Create your views here
# 127.0.0.1:8000/sms_cord/mobile/
from user.models import User
from rest_framework.generics import CreateAPIView

class SMSCodeView(APIView):
    def get(self,request,mobile):
        # 首先判断请求频率不能超过60秒
        conn = get_redis_connection('verify')
        flag = conn.get('sms_flge%s'%mobile)
        if flag:
            return Response({"message":"请求次数过去频繁"},status=404)
        #2生成随机验证码 并保存到数据库
        sms_code = "%06d" %random.randint(0,99999)

        pl= conn.pipeline()

        pl.setex('sms_cord_%s'%mobile,300,sms_code)
        pl.setex('sms_flge%s'%mobile,60,1)
        pl.execute()
        print(sms_code)
        # 3.用云通讯发送短信验证码
        # ccp = CCP()
        # ccp.send_template_sms(mobile,[sms_code,'5'],1)

        send_sms_code.delay(mobile,sms_code)
        # 4.返回对象
        print("hai")
        return Response({"message":"ok"})


class UserNameView(APIView):
    """判断手机号"""
    def get(self,request,username):
        # 查询用户数量
        count=User.objects.filter(username= username).count()
        #返回查询数量
        return Response({
            "username":username,
            "count":count
        })

class MobileView(APIView):
    """判断手机号"""
    def get(self,request,mobile):
        # 查询用户数量
        count=User.objects.filter(mobile= mobile).count()
        #返回查询数量
        return Response({
            "mobile":mobile,
            "count":count
        })


class UserView(CreateAPIView):
     # serializer_class =
    pass