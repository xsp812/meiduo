from django.shortcuts import render
from django_redis import get_redis_connection
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
import random

from goods.models import SKU
from goods.serializer import SKUSerializer
from meiduo.libs.yuntongxun.sms import CCP
from celery_tasks.sms.tasks import send_sms_code
# Create your views here
# 127.0.0.1:8000/sms_cord/mobile/
from user.models import User
from rest_framework.generics import UpdateAPIView,CreateAPIView,RetrieveAPIView,GenericAPIView
from user.serializer import UserSerializer, UserDetailSerializer, EmailUpdateSerializer, VerifyEmailSerializer, \
    AddUserBrowsingHistorySerializer


class SMSCodeView(APIView):
    def get(self, request, mobile):
        # 首先判断请求频率不能超过60秒
        conn = get_redis_connection('verify')
        flag = conn.get('sms_flge%s' % mobile)
        if flag:
            return Response({"message": "请求次数过去频繁"}, status=404)
        # 2生成随机验证码 并保存到数据库
        sms_code = "%06d" % random.randint(0, 99999)

        pl = conn.pipeline()

        pl.setex('sms_code_%s' % mobile, 300, sms_code)
        pl.setex('sms_flge%s' % mobile, 60, 1)
        pl.execute()
        print(sms_code)
        # 3.用云通讯发送短信验证码
        # ccp = CCP()
        # ccp.send_template_sms(mobile,[sms_code,'5'],1)

        send_sms_code.delay(mobile, sms_code)
        # 4.返回对象
        print("hai")
        return Response({"message": "ok"})


class UserNameView(APIView):
    """判断手机号"""

    def get(self, request, username):
        # 查询用户数量
        count = User.objects.filter(username=username).count()
        # 返回查询数量
        return Response({
            "username": username,
            "count": count
        })


class MobileView(APIView):
    """判断手机号"""

    def get(self, request, mobile):
        # 查询用户数量
        count = User.objects.filter(mobile=mobile).count()
        # 返回查询数量
        return Response({
            "mobile": mobile,
            "count": count
        })


class UserView(CreateAPIView):
    serializer_class = UserSerializer


class UserDetailView(RetrieveAPIView):
    serializer_class = UserDetailSerializer
    permission_classes = [IsAuthenticated]

    # def get(self,request):
        #获取参数
        #校验参数
    def get_object(self):
        print(self.request.user)
        return self.request.user



class EmailView(UpdateAPIView):
    """保存邮箱"""
    # serializer_class =
    # def put(self,request):
    #     #获取参数 获取邮箱
    #     #校验参数
    #     #更新保存到数据库
    #     #返回值处理
    permission_classes = [IsAuthenticated]
    serializer_class = EmailUpdateSerializer

    def get_object(self):

        return self.request.user

class VerifyEmailView(GenericAPIView):
    serializer_class = VerifyEmailSerializer
    def get(self,request):
        #获取参数
        data = request.query_params
        #校验参数
        ser = self.get_serializer(data= data)
        ser.is_valid()
        #修改邮箱验证状态
        data = ser.validated_data['data']
        id= data['id']
        try:
            user  = User.objects.get(id = id)
        except:
            return Response({'message':'没有查询到该用户'},status=400)
        user.email_active = True
        user.save()
        #返回值处理
        return Response({'email_active':'OK'})


class UserBrowsingHistoryView(CreateAPIView):
    """用户浏览历史记录"""
    # def post(self,request):
    serializer_class =AddUserBrowsingHistorySerializer
    permission_classes = [IsAuthenticated]

    def get(self,request):
        user_id = request.user.id
        conn = get_redis_connection('history')
        history = conn.lrange('history_%s'%user_id,0,5)
        skus=[]
        for sku_id in history:
            sku=SKU.objects.get(id = sku_id)
            skus.append(sku)

        ser =SKUSerializer(skus,many=True)

        return Response(ser.data)

