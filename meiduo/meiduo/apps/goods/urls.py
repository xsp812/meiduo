from django.conf.urls import url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^categories/(?P<pk>\d+)/skus/$',views.SKUView.as_view()),
    url(r'^categories/(?P<pk>\d+)/hotskus/$',views.SKUHotsView.as_view()),

]