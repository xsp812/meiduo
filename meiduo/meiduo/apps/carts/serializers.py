from rest_framework import serializers

from goods.models import SKU


class CartsSerializers(serializers.Serializer):

    sku_id = serializers.IntegerField(min_value=1)
    count = serializers.IntegerField()
    selected = serializers.BooleanField(default=True)

    def validate(self, attrs):
        #sku_id 验证
        try:
            sku=SKU.objects.get(id =attrs['sku_id'])
        except:
            raise serializers.ValidationError('商品不存在')

        if attrs['count'] >sku.stock:
            raise serializers.ValidationError('商品库存不足')
        return attrs

class CartSerializers(serializers.ModelSerializer):
    count = serializers.IntegerField(read_only=True)
    selected = serializers.BooleanField(read_only=True)
    class Meta:
        model = SKU
        fields = '__all__'